#! /usr/bin/env python
try:  # for pip >= 10
    from pip._internal.exceptions import InstallationError
    from pip._internal.req import parse_requirements
except ImportError:  # for pip <= 9.0.3
    from pip.exceptions import InstallationError
    from pip.req import parse_requirements

from setuptools import find_packages, setup


def convert_req_format(resource: str):
    egg, uri = resource.split("@")
    return "{uri}#egg={egg_name}".format(uri=uri, egg_name=egg)


try:
    requires = [
        str(ir.req)
        for ir in parse_requirements("requirements.txt", session="No Session")
    ]
except InstallationError:
    requires = list()

dependency_links = [convert_req_format(req) for req in requires if "@" in req]
tests_require = ["pytest", "pytest-cov"]

setup(
    name="{{cookiecutter.project_name}}",
    use_scm_version=True,
    description="{{cookiecutter.python_project_description}}",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Console",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
    ],
    author="{{cookiecutter.author}}",
    author_email="{{cookiecutter.author_email}}",
    license="{{cookiecutter.license}}",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    dependency_links=dependency_links,
    install_requires=requires,
    setup_requires=["setuptools-scm"],
    tests_require=tests_require,
)
